[GtkTemplate (ui = "/org/example/App/window.ui")]
public class CsDialog.Window : Adw.ApplicationWindow {
    [GtkChild]
    private unowned DialogHost host;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    [GtkCallback]
    private void show_dialog () {
        host.show_dialog ();
    }
}
