public class CsDialog.LayoutSqueezer : Gtk.Widget, Gtk.Orientable {
    private Gtk.LayoutManager[] managers;

    private int _current_index;
    public int current_index {
        get { return _current_index; }
        set {
            if (current_index == value)
                return;

            _current_index = value;

            if (0 <= current_index < managers.length)
                set_current_manager (managers[current_index]);
            else
                set_current_manager (current_layout);
        }
    }

    private double progress;
    private Gtk.LayoutManager current_layout;
    private Gtk.LayoutManager previous_layout;
    private Animation? animation;
    private bool has_layout;

    construct {
        managers = {};
    }

    private void set_current_manager (Gtk.LayoutManager layout) {
        if (layout == current_layout)
            return;

        if (animation != null)
            animation.stop ();

        previous_layout = current_layout;
        current_layout = layout;

        if (previous_layout != null && current_layout != null) {
            if (!has_layout) {
                progress = 1;
                queue_resize ();
                has_layout = true;
                return;
            }

            progress = 0;
            animation = new Animation (this, 0, 1, 250);
            animation.notify["value"].connect (() => {
                progress = animation.value;
                queue_resize ();
            });
            animation.done.connect (() => {
                animation = null;
            });
            animation.start ();
        } else {
            queue_resize ();
        }
    }

    public void append_layout_manager (Gtk.LayoutManager layout) {
        managers += layout;

        if (current_layout == null) {
            current_layout = layout;
            queue_resize ();
        }
    }

    public Gtk.LayoutChild get_layout_child (Gtk.LayoutManager layout, Gtk.Widget widget) {
        layout_manager = layout;
        var child = layout.get_layout_child (widget);
        layout_manager = null;

        return child;
    }


    private static inline double lerp (double a, double b, double t) {
        return a * (1 - t) + b * t;
    }

    // TODO queue_resize ()
    public Gtk.Orientation orientation { get; set; }

    protected override void measure (Gtk.Orientation orientation, int for_size, out int minimum, out int natural, out int minimum_baseline, out int natural_baseline) {
        if (current_layout == null) {
            minimum = 0;
            natural = 0;
            minimum_baseline = -1;
            natural_baseline = -1;

            return;
        }

        if (orientation == this.orientation) {
            layout_manager = managers[0];
            layout_manager.measure (this, orientation, for_size, out minimum, null, out minimum_baseline, null);
            layout_manager = null;

            layout_manager = managers[managers.length - 1];
            layout_manager.measure (this, orientation, for_size, null, out natural, null, out natural_baseline);
            layout_manager = null;

            return;
        }

        if (animation == null) {
            layout_manager = current_layout;
            current_layout.measure (this, orientation, for_size, out minimum, out natural, out minimum_baseline, out natural_baseline);
            layout_manager = null;

            return;
        }

        int prev_min, prev_nat, prev_min_baseline, prev_nat_baseline;
        layout_manager = previous_layout;
        previous_layout.measure (this, orientation, for_size, out prev_min, out prev_nat, out prev_min_baseline, out prev_nat_baseline);
        layout_manager = null;

        int min, nat, min_baseline, nat_baseline;
        layout_manager = current_layout;
        current_layout.measure (this, orientation, for_size, out min, out nat, out min_baseline, out nat_baseline);
        layout_manager = null;

        minimum          = (int) lerp (prev_min,          min,          progress);
        natural          = (int) lerp (prev_nat,          nat,          progress);
        minimum_baseline = (int) lerp (prev_min_baseline, min_baseline, progress);
        natural_baseline = (int) lerp (prev_nat_baseline, nat_baseline, progress);
    }

    private struct WidgetData {
        int width;
        int height;
        int baseline;
        Graphene.Matrix transform;
    }

    private inline Graphene.Matrix get_allocated_transform (Gtk.Widget child) {
        Graphene.Matrix ret;

        child.compute_transform (this, out ret);

        var context = child.get_style_context ();
        var margin = context.get_margin ();
        var padding = context.get_padding ();
        var border = context.get_border ();

        ret.translate ({
            -margin.left - padding.left - border.left - child.margin_start,
            -margin.top  - padding.top  - border.top - child.margin_top,
            0
        });

        return ret;
    }

    protected override void size_allocate (int width, int height, int baseline) {
        if (current_layout == null) {
            for (var c = get_first_child (); c != null; c = c.get_next_sibling ())
                c.allocate (width, height, baseline, null);

            return;
        }

        // TODO: binary search
        // TOOD: check is sizes changed or something else
        for (int i = managers.length - 1; i >= 0; i--) {
            var manager = managers[i];
            int min;

            layout_manager = manager;
            manager.measure (this, orientation, -1, out min, null, null, null);
            layout_manager = null;

            if (min <= (orientation == Gtk.Orientation.HORIZONTAL ? width : height)) {
                set_current_manager (manager);
                break;
            }
        }

        if (animation == null) {
            layout_manager = current_layout;
            current_layout.allocate (this, width, height, baseline);
            layout_manager = null;

            return;
        }
/*
        layout_manager = previous_layout;
        previous_layout.measure (this, width, height, baseline);
        previous_layout.allocate (this, width, height, baseline);
        layout_manager = null;
*/
        WidgetData[] from = {};
        for (var c = get_first_child (); c != null; c = c.get_next_sibling ()) {
            WidgetData data = {
                c.get_allocated_width () + c.margin_start + c.margin_end,
                c.get_allocated_height () + c.margin_top + c.margin_bottom,
                c.get_allocated_baseline (),
                get_allocated_transform (c),
            };
            from += data;
        }

        layout_manager = current_layout;
        current_layout.allocate (this, width, height, baseline);
        layout_manager = null;

        WidgetData[] to = {};
        for (var c = get_first_child (); c != null; c = c.get_next_sibling ()) {
            WidgetData data = {
                c.get_allocated_width () + c.margin_start + c.margin_end,
                c.get_allocated_height () + c.margin_top + c.margin_bottom,
                c.get_allocated_baseline (),
                get_allocated_transform (c),
            };
            to += data;
        }

        int i = 0;
//        int n = to.length;
        for (var c = get_first_child (); c != null; c = c.get_next_sibling ()) {
            var t = progress; //((progress * n) - (double) i).clamp (0, 1);
            int w = (int) lerp (from[i].width,    to[i].width,    t);
            int h = (int) lerp (from[i].height,   to[i].height,   t);
            int b = (int) lerp (from[i].baseline, to[i].baseline, t);

            var transform = from[i].transform.interpolate (to[i].transform, t);

            c.allocate (w, h, b, new Gsk.Transform ().matrix (transform));

            i++;
        }
    }

    protected override Gtk.SizeRequestMode get_request_mode () {
        return Gtk.SizeRequestMode.CONSTANT_SIZE;
    }

    protected override void dispose () {
        var child = get_first_child ();
        while (child != null) {
            var c = child;
            child = child.get_next_sibling ();

            c.unparent ();
        }

        base.dispose ();
    }
}
